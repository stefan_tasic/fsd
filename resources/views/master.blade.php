<html>
    <head>
        <link rel="stylesheet" href="/css/bootstrap.css">
        <script src="/js/jquery-2.1.4.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        @yield("head")
    </head>
    <body>
        <div class="container">
            <div class="row">
                <?php
                    if(Session::get("user"))
                    {
                        echo '<div class="col-md-2">Dobrodosli, '.Session::get("user").'</div>';
                        echo '<div class="col-md-2"><a href="/knjige">Knjige</a></div>';
                        echo '<div class="col-md-2"><a href="/autori">Autori</a></div>';
                        echo '<div class="col-md-2"><a href="/knjige/create">Dodaj knjigu</a></div>';
                        echo '<div class="col-md-2"><a href="/autori/create">Dodaj autora</a></div>';
                        echo '<div class="col-md-2"><a href="/logout">Logout</a>';
                    }
                    else
                    {
                        echo '<div class="col-md-2"><a href="/knjige">Knjige</a></div>';
                        echo '<div class="col-md-2"><a href="/autori">Autori</a></div>';
                        echo '<div class="col-md-2"><a href="/register">Register</a></div>';
                        echo '<div class="col-md-2"><a href="/login">Login</a></div>';
                    }
                ?>
            </div>
        </div>
        @yield("content")
    </body>
</html>