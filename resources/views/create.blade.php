@extends("master")
@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                {!! Form::open(['route' => ['index.store'],'method' => 'post']) !!}
                    <label for="naziv">Naziv:</label>
                    {!! Form::text('naziv', '', array('class' => 'form-control')) !!}
                    <label for="autor">Autor:</label>
                    <select name="autor" class="form-control">
                        @foreach($autori as $autor)
                            <option value="{{$autor->id}}">{{$autor->ime}} {{$autor->prezime}}</option>
                        @endforeach
                    </select>
                    <label for="god_izdavanja">God izdavanja:</label>
                    {!! Form::number('god_izdavanja', '', array('class' => 'form-control')) !!}
                    <label for="jezik">Jezik:</label>
                    {!! Form::text('jezik', '', array('class' => 'form-control')) !!}
                    <label for="org_jezik">Orig jezik:</label>
                    {!! Form::text('org_jezik', '', array('class' => 'form-control')) !!}
                    {!! Form::submit('Dodaj',array('class' => 'btn btn-primary')) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection