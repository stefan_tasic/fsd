@extends("master")
@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                {!! Form::open(['route' => ['index.update'],'method' => 'put']) !!}
                    {!! Form::hidden('id', $knjiga->id) !!}
                    <label for="naziv">Naziv:</label>
                    {!! Form::text('naziv', $knjiga->naziv, array('class' => 'form-control')) !!}
                    <label for="autor">Autor:</label>
                    <select name="autor" class="form-control">
                        @foreach($autori as $autor)
                            <option value="{{$autor->id}}" @if($autor->id == $knjiga->autor) selected @endif>{{$autor->ime}} {{$autor->prezime}}</option>
                        @endforeach
                    </select>
                    <label for="god_izdavanja">God izdavanja:</label>
                    {!! Form::number('god_izdavanja', $knjiga->god_izdavanja, array('class' => 'form-control')) !!}
                    <label for="jezik">Jezik:</label>
                    {!! Form::text('jezik', $knjiga->jezik, array('class' => 'form-control')) !!}
                    <label for="org_jezik">Orig jezik:</label>
                    {!! Form::text('org_jezik', $knjiga->org_jezik, array('class' => 'form-control')) !!}
                    {!! Form::submit('Izmeni',array('class' => 'btn btn-primary')) !!}
                    @if ($errors->has('success')) <p class="help-block">{{ $errors->first('success') }}</p> @endif
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection