@extends("master")
@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                {!! Form::open(['route' => ['autori.update',$autor->id],'method' => 'put']) !!}
                    <label for="ime">Ime:</label>
                    {!! Form::text('ime', $autor->ime, array('class' => 'form-control')) !!}
                    <label for="prezime">Prezime:</label>
                    {!! Form::text('prezime', $autor->prezime, array('class' => 'form-control')) !!}
                    <label for="god_rodjenja">God rodjenja:</label>
                    {!! Form::number('god_rodjenja', $autor->god_rodjenja, array('class' => 'form-control')) !!}
                    {!! Form::submit('Izmeni',array('class' => 'btn btn-primary')) !!}
                    @if ($errors->has('success')) <p class="help-block">{{ $errors->first('success') }}</p> @endif
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection