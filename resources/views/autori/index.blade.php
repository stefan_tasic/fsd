@extends("master")
@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-2">Ime:</div>
            <div class="col-md-2">Prezime:</div>
            <div class="col-md-2">God rodjenja:</div>
        </div>
        @foreach($autori as $key => $autor)
            <div class="row">
                <div class="col-md-2">{{$autor->ime}}</div>
                <div class="col-md-2">{{$autor->prezime}}</div>
                <div class="col-md-2">{{$autor->god_rodjenja}}</div>
                <?php
                    if(Session::get("user"))
                    {
                ?>
                    <div class="col-md-1">
                        {!! Form::open(['route' => ['autori.destroy', $autor->id],'method' => 'delete']) !!}
                            {!! Form::submit('Obrisi',array('class' => 'btn btn-primary')) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-1">
                        <a href="/autori/{{$autor->id}}/edit">Izmeni</a>
                    </div>
                <?php
                    }
                ?>
            </div>
        @endforeach
    </div>
@endsection