@extends("master")
@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                {!! Form::open(['route' => ['autori.store'],'method' => 'post']) !!}
                    <label for="ime">Ime:</label>
                    {!! Form::text('ime', '', array('class' => 'form-control')) !!}
                    <label for="prezime">Prezime:</label>
                    {!! Form::text('prezime', '', array('class' => 'form-control')) !!}
                    <label for="god_rodjenja">God rodjenja:</label>
                    {!! Form::number('god_rodjenja', '', array('class' => 'form-control')) !!}
                    {!! Form::submit('Dodaj',array('class' => 'btn btn-primary')) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection