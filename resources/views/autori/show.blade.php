@extends("master")
@section("content")
    <div class="container">
        <div class="row">
            Ime: {{$autor->ime}}
            <br/>
            Prezime: {{$autor->prezime}}
            <br/>
            God rodjenja: {{$autor->god_rodjenja}}
            <?php
                if(Session::get("user"))
                {
            ?>
                <div class="row">
                    <div class="col-md-2">
                        {!! Form::open(['route' => ['autori.destroy', $autor->id],'method' => 'delete']) !!}
                            {!! Form::submit('Obrisi',array('class' => 'btn btn-primary')) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-2">
                        <a href="/autori/{{$autor->id}}/edit">Izmeni</a>
                    </div>
                </div>
            <?php
                }
            ?>
        </div>
    </div>
@endsection