@extends("master")
@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <form method="get" action="<?php echo explode("?",$_SERVER['REQUEST_URI'])[0]?>">
                    <input type="number" min="1900" max="2015" value="<?php if($search !="") echo $search ?>" id="search" name="search" class="form-control" placeholder="Pretraga po godini" style="margin-top: 25px">
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">Naziv:</div>
            <div class="col-md-2">Autor:</div>
            <div class="col-md-2">God izdavanja:</div>
            <div class="col-md-2">Jezik:</div>
            <div class="col-md-2">Orig jezik:</div>
        </div>
        @foreach($knjige as $key => $knjiga)
            <div class="row">
                <div class="col-md-2">{{$knjiga->naziv}}</div>
                <div class="col-md-2"><a href='/autori/{{$knjiga->autor_id}}'>{{$knjiga->autor}}</a></div>
                <div class="col-md-2">{{$knjiga->god_izdavanja}}</div>
                <div class="col-md-2">{{$knjiga->jezik}}</div>
                <div class="col-md-2">{{$knjiga->org_jezik}}</div>
                <?php
                    if(Session::get("user"))
                    {
                ?>
                    <div class="col-md-1">
                        {!! Form::open(['route' => ['index.destroy', $knjiga->id],'method' => 'delete']) !!}
                            {!! Form::submit('Obrisi',array('class' => 'btn btn-primary')) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-1">
                        <a href="/knjige/{{$knjiga->id}}/edit">Izmeni</a>
                    </div>
                <?php
                    }
                ?>
            </div>
        @endforeach
        <center>
            <?php
                if(isset($_GET["search"]))
                    echo $knjige->appends(['search' => $_GET["search"]])->render();
                else
                    echo $knjige->render();
            ?>
        </center>
    </div>
@endsection