@extends("master")
@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                {!! Form::open(array('url' => '/login','method' => 'post')) !!}
                    <label for="username">Username:</label>
                    {!! Form::text('username', old('username'), array('class' => 'form-control','id'=>'username')) !!}
                    <label for="password">Password:</label>
                    {!! Form::password('password', array('class' => 'form-control','id'=>'password')) !!}
                    @if ($errors->has('wrong')) <p class="help-block">{{ $errors->first('wrong') }}</p> @endif
                    {!! Form::submit('Log in',array('class' => 'btn btn-primary')) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection