@extends("master")
@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                {!! Form::open(array('url' => '/register','method' => 'post')) !!}
                    <label for="username">Username:</label>
                    {!! Form::text('username', old('username'), array('class' => 'form-control','id'=>'username')) !!}
                    @if ($errors->has('username')) <p class="help-block">{{ $errors->first('username') }}</p> @endif
                    <label for="password">Password:</label>
                    {!! Form::password('password', array('class' => 'form-control','id'=>'password')) !!}
                    @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
                    <label for="password_confirmation">Confirm password:</label>
                    {!! Form::password('password_confirmation', array('class' => 'form-control','id'=>'password_confirmation')) !!}
                    {!! Form::submit('Register',array('class' => 'btn btn-primary')) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection