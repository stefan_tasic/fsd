<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\korisnikModel;
use Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function __construct()
    {
		//provera da li je korisnik ulogovan, ukoliko jeste, ne moze koristiti funkcije za logovanje i registrovanje
        $this->beforeFilter(function(){
            if(Session::get("user"))
            {
                abort(503);
            }
        });
    }
    public function register()
    {
        return view("register");
    }
	//registracija korisnika
    public function register_post()
    {
        $request = Request::all(); //vraca potrebne parametre

        //validaciona provera 
        $errors = Validator::make($request, [
            'username' => 'required|min:6|max:30|unique:korisnik',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ]);
		//ukoliko ima greska, vraca korisnika na register stranu sa porukom o gresci
        if ($errors->fails())
        {
            return redirect('register')->withErrors($errors->messages())->withInput();
        }
        else
        {
			//kreiranje novog korisnika
            $korisnik = new korisnikModel();
            $korisnik->username = $request["username"];
            $korisnik->password = hash("sha256",$request["password"]);
            $korisnik->save();

			//kreiranje sesije za logovanje, i prebacivanje na stranu /knjige
            Session::put("user",$korisnik->username);
            return redirect("knjige");
        }
    }
    public function login()
    {
        return view("login");

    }
	//logovanje korisnika
    public function login_post()
    {
        //get post data
        $request = Request::all();

        $korisnik = korisnikModel::where("username",$request["username"])->where("password",hash('sha256',$request["password"]))->first();
        if($korisnik)
        {
			//ukoliko korisnik postoji, setujemo sesiju
			Session::put('user',$korisnik->username);
            return redirect("knjige");
        }
        else
        {
            //ukoliko ne postoji izbaciti poruku
            $error["wrong"] = "Username or password doesn't match";
            return redirect('login')->withErrors($error)->withInput();
        }

    }

}
