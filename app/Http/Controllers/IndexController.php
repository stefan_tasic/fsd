<?php

namespace App\Http\Controllers;


use App\autoriModel;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\knjigeModel;
use Illuminate\Support\Facades\Session;
use Request;
use DB;
class IndexController extends Controller
{
    public function index()
    {
        $request = Request::all();

        if(isset($request["search"]))
        {
            $search = $request["search"];
            $knjige = knjigeModel::where("god_izdavanja",$search)
                ->join("autori","autori.id","=","knjige.autor")
                ->select(DB::raw("knjige.*,concat(autori.ime,' ',autori.prezime) as autor"))
                ->paginate(10);
        }
        else
        {
            $knjige = knjigeModel::join("autori","autori.id","=","knjige.autor")
                ->select(DB::raw("knjige.*,concat(autori.ime,' ',autori.prezime) as autor, autori.id as autor_id"))
                ->paginate(10);
            $search = "";
        }
        return response()->json($knjige);

    }
    public function create()
    {
        if(!Session::get("user"))
            abort(403);
        $autori = autoriModel::all();
        return view("create")
            ->with("autori",$autori);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Session::get("user"))
            abort(403);
        $request = Request::all();
        $knjiga = new knjigeModel();
        $knjiga->naziv = $request["naziv"];
        $knjiga->autor = $request["autor"];
        $knjiga->god_izdavanja = $request["god_izdavanja"];
        $knjiga->jezik = $request["jezik"];
        $knjiga->org_jezik = $request["org_jezik"];
        $knjiga->save();
        $error["success"] = "Uspesno ste dodali knjigu";
        return redirect("knjige/" . $knjiga->id . "/edit")->withErrors($error);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Session::get("user"))
            abort(403);
        $knjiga = knjigeModel::where("id",$id)->first();
        $autori = autoriModel::all();
        return view("edit")
            ->with("knjiga",$knjiga)
            ->with("autori",$autori);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Session::get("user"))
            abort(403);
        $request = Request::all();
        $knjiga = knjigeModel::where("id",$request["id"])->first();
        $knjiga->naziv = $request["naziv"];
        $knjiga->autor = $request["autor"];
        $knjiga->god_izdavanja = $request["god_izdavanja"];
        $knjiga->jezik = $request["jezik"];
        $knjiga->org_jezik = $request["org_jezik"];
        $knjiga->update();
        $error["success"] = "Uspesno ste izmenili knjigu";
        return redirect("knjige/" . $request['id'] . "/edit")->withErrors($error);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Session::get("user"))
            abort(403);
        knjigeModel::where("id",$id)->delete();
        return redirect("knjige");
    }
}
