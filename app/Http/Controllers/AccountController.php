<?php

namespace App\Http\Controllers;

use Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AccountController extends Controller
{
    public function __construct()
    {
        //provera da li je korisnik ulogovan
        $this->beforeFilter(function(){
            if(!Session::get("user"))
            {
                return redirect("/");
            }
        });
    }
	//logout korisnika
    public function logout()
    {
        Session::forget("user");

        return redirect("/");
    }
}
