<?php

namespace App\Http\Controllers;

use App\autoriModel;
use Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
class AutorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	//vraca sve autore
    public function index()
    {
        $autori = autoriModel::paginate(10);
        return view("autori.index")
            ->with("autori",$autori);
    }
	//otvara stranicu za kreiranje autora
    public function create()
    {
        if(!Session::get("user"))
            abort(403);
        return view("autori.create");
    }
	//upisuje novog autora u bazu i vraca ga na stranu za pregled tog korisnika
    public function store(Request $request)
    {
        if(!Session::get("user"))
            abort(403);
        $request = Request::all();
        $autor = new autoriModel();
        $autor->ime = $request["ime"];
        $autor->prezime = $request["prezime"];
        $autor->god_rodjenja = $request["god_rodjenja"];
        $autor->save();


        $error["success"] = "Uspesno ste dodali autora";
        return redirect("autori/" . $autor->id . "/edit")->withErrors($error);
    }
	//prikazuje jednog autora
    public function show($id)
    {
        $autor = autoriModel::where("id",$id)->first();
        return view("autori.show")
            ->with("autor",$autor);
    }

	//otvara stranu za izmenu autora
    public function edit($id)
    {
        if(!Session::get("user"))
            abort(403);
        $autor = autoriModel::where("id", $id)->first();
        return view("autori.edit")
            ->with("autor", $autor);
    }

	//azurira autora u bazi 
    public function update(Request $request, $id)
    {
        if(!Session::get("user"))
            abort(403);
        $request = Request::all();
        $autor = autoriModel::where("id",$id)->first();

        $autor->ime = $request["ime"];
        $autor->prezime = $request["prezime"];
        $autor->god_rodjenja = $request["god_rodjenja"];
        $autor->update();


        $error["success"] = "Uspesno ste izmenili autora";
        return redirect("autori/" . $autor->id . "/edit")->withErrors($error);
    }
	//brisanje autora
    public function destroy($id)
    {
        if(!Session::get("user"))
            abort(403);
        autoriModel::where("id",$id)->delete();
        return redirect("autori");
    }
}
